# copied from https://github.com/gladiusio/gladius-edge-docker/blob/master/Dockerfile-edge
FROM node:9-alpine

# expose ports for the edge daemon
EXPOSE 5000/tcp
EXPOSE 8080/tcp

# install dependencies and setup npm directory for
# node user
RUN apk add --no-cache git \
 && mkdir /npm \
 && chown node:node /npm \
 # in addition we setup a cdn content directory
 # which shall make the split of content and application
 # easier (untested)
 && mkdir /cdn \
 && chown node:node /cdn

# become user node and setup installation directory
USER node
ENV PATH=/npm/bin:$PATH
ENV NPM_CONFIG_PREFIX=/npm

# install the edge daemon
RUN npm install gladius-edge-daemon -g 
	# TODO: add entrypoint script which moves the cdn content or else volume mount will overwrite the default contents
	# after the installation we need to move the contents of the cdn_content directory
	# to the /cdn dir and create a softlink to it
	#&& mv /npm/lib/node_modules/gladius-edge-daemon/cdn_content/* /cdn/ \
	#&& rmdir /npm/lib/node_modules/gladius-edge-daemon/cdn_content \
	#&& ln -s /cdn /npm/lib/node_modules/gladius-edge-daemon/cdn_content

# start the edge daemon 
CMD [ "gladius-edge" ]